<?php /*** Bismillahirrahmanirrahim ***/

namespace KakakAsuh\Institution;

use Pusaka\Tanur\Support\TanurServiceProvider;

/**
 * ServiceProvider
 */
class ServiceProvider extends TanurServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    public function register()
    {
        parent::register();
    }

    /**
     * Get provided services for deferred service provider
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}